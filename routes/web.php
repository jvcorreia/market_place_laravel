<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get
//Route::post
//Route::put
//Route::delete
//Route::patch
//Route::options
Route::get('/', 'HomeController@index')->name('home');
Route::get('/product/{slug}', 'HomeController@single')->name('product.single');
Route::get('/cat/{cat}', 'Admin\\CategoryController@cat')->name('cat');

Route::prefix('cart')->name('cart.')->group(function (){

    Route::post('add','CartController@add')->name('add');
    Route::get('/','CartController@index')->name('index');
    Route::get('cancel','CartController@cancel')->name('cancel');
    Route::get('remove/{slug}','CartController@remove')->name('remove');

});

Route::prefix('checkout')->name('checkout.')->group(function (){
    Route::get('/','CheckoutController@index')->name('index');
});

//Route::get('admin/stores','Admin\\StoreController@index'); sem o route::prefixe
Route::prefix('admin')->name('admin.')->namespace('Admin')->group(function (){//Para diminuir a redundancia

    Route::prefix('stores')->name('stores.')->group(function (){
        Route::get('/','StoreController@index')->name('index');//é possível colocar um apelido com o ->name
        Route::get('/create','StoreController@create')->name('create');
        Route::get('/{slug}','StoreController@single')->name('single');
        Route::post('/store','StoreController@store')->name('store');
        Route::get('/{store}/edit','StoreController@edit')->name('edit');
        Route::get('/delete/{store}','StoreController@delete')->name('delete');
        Route::post('/update/{store}','StoreController@update')->name('update');
    });

    Route::resource('categories','CategoryController');
    Route::post('photos/remove/', 'ProductPhotoController@remove_photo')->name('photo.remove');

    //Route::resource('products','ProductsController'); caso estivesse trabalhando assim, teria que transformar as ações em
    //que estão com put e delete em forms, com o método post

    Route::prefix('products')->name('products.')->group(function (){
        Route::get('/','ProductsController@index')->name('index');//é possível colocar um apelido com o ->name
        Route::get('/create','ProductsController@create')->name('create');
        Route::post('/store','ProductsController@store')->name('store');
        Route::get('/{product}/edit','ProductsController@edit')->name('edit');
        Route::get('/destroy/{product}','ProductsController@destroy')->name('destroy');
        Route::post('/update/{product}','ProductsController@update')->name('update');
    });

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



/*
Route::get('/model',function (){
   //$products =  \App\Product::all(); //Select * from products

    //$user = new \App\User();
    //Active record
   // $user = \App\User::find(1);
    //$user->name = 'alou';
    //$user->email = 'alou@aloua';
    //$user->password = bcrypt('12334578');
    //$user->save();

    //return \App\User::find(1);
    //return \App\User::where('name','alou')->get();

    /*
    $user = \App\User::create([
       'name' => 'jonnys',
       'email' => 'obrabo@brabo',
       'password' => bcrypt('121854'),
    ]);
    dd($user);
    return $user;


    $user = \App\User::find(42);
    $user->update([
        'name' => 'novo nome'
    ]);

    return \App\User::all();
});*/


