@extends('layouts.front')
@section('content')
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Compra efetuada!</h4>
        <p>Muito bem, se você chegou até aqui certamente conseguiu fechar uma compra.</p>
        <hr>
        <p class="mb-0">O seu carrinho foi zerado e iremos preparar o seu pedido para envio.</p>
    </div>
@endsection
