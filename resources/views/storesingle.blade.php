@extends('layouts.front')
@section('content')
    <div class="row">
        <a href="{{route('home')}}" class="btn btn-info">Voltar</a>

        <div class="col-md-12">
            @if($store->logo != null)
                <img src="{{asset('storage/'.$store->logo)}}" style="width: 70%;height: 150px;margin-left: 15%" alt="">
            @else
                <img src="{{asset('assets/no-photo.jpg')}}" style="width: 70%;height: 150px;margin-left: 15%" alt="">
            @endif

        </div>
        <div class="col-md-8">
            <h2>{{$store->name}}</h2>
            <p >{{$store->description}}</p>

        </div>

    </div>

@endsection
