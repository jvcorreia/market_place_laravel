@extends('layouts.front')

@section('content')
    <h2>{{$category->name}}</h2>
    <div class="row">

        @foreach($category->products as $key => $p)

            <div class="col-md-4">
                <div class="card">
                    @if($p->photos->count())
                        <img src="{{asset('storage/'.$p->photos->first()->image)}}" style="width: 90%;height: 150px;margin-left: 5%" alt="">
                    @else
                        <img src="{{asset('assets/no-photo.jpg')}}" style="width: 90%;height: 150px;margin-left: 5%" alt="">
                    @endif
                    <div class="card-body">
                        <h2 class="card-title">{{$p->name}}</h2>
                        <p class="card-text">{{$p->description}}</p>
                        @if(isset($p->slug))
                            <a href="{{route('product.single',['slug'=>$p->slug])}}" class="btn btn-success">Info</a>
                        @endif
                    </div>
                </div>
            </div>
            @if(($key+1)%3 == 0)
    </div><div class="row front">
        @endif

        @endforeach
    </div>



@endsection
