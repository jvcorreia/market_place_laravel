@extends('layouts.front')
@section('content')
    <div class="row">
        <a href="{{route('home')}}" class="btn btn-info">Voltar</a>

            <div class="col-md-12">
                    @if($product->photos->count())
                        <img src="{{asset('storage/'.$product->photos->first()->image)}}" style="width: 70%;height: 150px;margin-left: 15%" alt="">
                    @else
                        <img src="{{asset('assets/no-photo.jpg')}}" style="width: 70%;height: 150px;margin-left: 15%" alt="">
                    @endif

            </div>
        <div class="col-md-8">
            <h2>{{$product->name}}</h2>
            <p >{{$product->description}}</p>
            <p >R$ {{number_format($product->price,'2', ',','.')}}</p>
            <p >Loja: {{$product->store->name}}</p>
        </div>
        <div class="col-md-4" style="float: right">
            <form method="post" action="{{route('cart.add')}}">
            @csrf
                <input type="hidden" name="product[name]" value="{{$product->name}}">
                <input type="hidden" name="product[price]" value="{{$product->price}}">
                <input type="hidden" name="product[slug]" value="{{$product->slug}}">
                <div class="form-group" >
                    <label>Quantidade</label>
                    <input name="product[amount]" type="number" class="form-control col-md-2" required value="1">
                </div>
            <button type="submit" style="color: #f8fafc" class="btn btn-lg btn-danger">Comprar</button>
            </form>
        </div>

    </div>

@endsection
