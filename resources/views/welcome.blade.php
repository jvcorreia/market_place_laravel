@extends('layouts.front')

@section('content')
<div class="row">

    @foreach($products as $key => $p)

        <div class="col-md-4">
            <div class="card">
            @if($p->photos->count())
             <img src="{{asset('storage/'.$p->photos->first()->image)}}" style="width: 90%;height: 150px;margin-left: 5%" alt="">
                @else
                    <img src="{{asset('assets/no-photo.jpg')}}" style="width: 90%;height: 150px;margin-left: 5%" alt="">
            @endif
                <div class="card-body">
                    <h2 class="card-title">{{$p->name}}</h2>
                     <p class="card-text">{{$p->description}}</p>
                    @if(isset($p->slug))
                        <a href="{{route('product.single',['slug'=>$p->slug])}}" class="btn btn-success">Info</a>
                    @endif
                </div>
            </div>
        </div>
                @if(($key+1)%3 == 0)
                    </div><div class="row front">
                @endif

    @endforeach
</div>

    <div class="row">
        <div class="col-12">
            <h2>Lojas Destaque</h2>
            <hr>
        </div>
        @foreach($stores as $s)
            <div class="col-md-4">
                @if($s->logo != null)
                <img src="{{asset('storage/'.$s->logo)}}" alt="Logo">
                @else
                    <img src="{{asset('assets/no-logo.png')}}" style="width: 90%;height: 150px;margin-left: 5%" alt="">
                @endif
                <h3>{{$s->name}}</h3>
                <p>{{$s->description}}</p>
                    <a href="{{route('admin.stores.single',['slug'=>$s->slug])}}">Mais informações</a>
            </div>
        @endforeach

    </div>

@endsection
