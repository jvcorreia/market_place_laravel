@extends('layouts.front')
@section('content')


    @if((session()->has('cart')))
@php
    $total = 0;
@endphp
    @foreach(session()->get('cart') as $item)
       @php
           $total += $item['price']*$item['amount']
       @endphp
        <div class="row">
            <div class="list-group" style="width: 80%;background-color: #3490dc" >

                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">{{$item['name']}}</h5>
                        <a href="{{route('cart.remove',['slug'=> $item['slug']])}}" class="btn btn-danger">Remover</a>
                    </div>
                    <p>Quantidade: {{$item['amount']}}</p>
                    <p class="mb-1">R${{number_format($item['price']*$item['amount'],'2', ',','.')}}</p>

            </div>
        </div>
    @endforeach
    <div class="row">
        <div class="col-md-3" style="right: 1.2%">
        <li class="list-group-item list-group-item-success">O total é:  R${{number_format($total,'2', ',','.')}}</li>
        </div>
        <div class="col-md-8" style="margin-top: 1%">
            <a style="color:whitesmoke;" class="btn btn-success" href="{{route('checkout.index')}}">Concluir compra</a>
            <a style="color: #f8fafc" class="btn btn-danger" href="{{route('cart.cancel')}}">Desistir da compra</a>
        </div>
    </div>
    @else
    <h1 style="text-align: center">Sem itens no carrinho</h1>
    @endif
@endsection
