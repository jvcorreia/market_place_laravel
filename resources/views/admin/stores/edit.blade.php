@extends('layouts.app')

@section('content')
    <h1>Editar Loja</h1>
    <form action="{{route('admin.stores.update',['store'=>$store->id])}}" method="post" enctype="multipart/form-data">

        <div class="form-group">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <label>Nome Loja </label>
            <input type="text" name="name" class="form-control" value="{{$store->name}}">
        </div>
        <div class="form-group">
            <label>Descrição </label>
            <input type="text" name="description" class="form-control" value="{{$store->description}}">
        </div>
        <div class="form-group">
            <label> Telefone</label>
            <input type="text" name="phone" class="form-control" value="{{$store->phone}}">
        </div>
        <div class="form-group">
            <label>Celular/WPP </label>
            <input type="text" name="mobile_phone" class="form-control" value="{{$store->mobile_phone}}">
        </div>

        <div class="form-group">
            <p>
                <img src="{{asset('storage/'.$store->logo)}}">
            </p>
            <label>Logo</label>
            <input type="file" name="photos" class="form-control @error('photos') is-invalid @enderror">
        </div>



        <div>
            <button type="submit" class="btn btn-success">Atualizar Loja</button>
        </div>
    </form>


@endsection
