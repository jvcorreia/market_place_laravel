@extends('layouts.app')

@section('content')
    @if(!$store)
    <a href="{{route('admin.stores.create')}}"  class="btn btn-sm btn-success"> Criar loja</a>
    @endif

    @if($store)
<table class="table table-striped">
    <thead>
    <tr>
       <th>#</th>
        <th>Loja</th>
        <th>Total de produtos</th>
        <th>Ações</th>
    </tr>
    </thead>
    <tbody>

        <tr>
            <td>{{$store->id}}</td>
            <td>{{$store->name}}</td>
            <td>{{$store->products->count()}}</td>
            <td>
                <a href="{{route('admin.stores.edit',['store'=>$store->id])}}"  class="btn btn-sm btn-primary"> Editar</a>
                <a href="{{route('admin.stores.delete',['store'=>$store->id])}}"  class="btn btn-sm btn-danger"> Deletar</a>



            </td>
        </tr>

    </tbody>
</table>
    @endif



@endsection
