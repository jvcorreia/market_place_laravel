@extends('layouts.app')

@section('content')

<h1>
    Mostrando produto
</h1>
    <table class="table table-stripped">
        <thead>
        <td>ID</td>
        <td>Nome</td>
        <td>Preço</td>

        </thead>
        <tbody>
        <td>{{$data->id}}</td>
        <td>{{$data->name}}</td>
        <td>{{$data->price}}</td>
        </tbody>
    </table>


@endsection
