@extends('layouts.app')

@section('content')
    <a href="{{route('admin.products.create')}}"  class="btn btn-sm btn-success"> Criar Produto</a>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Nome</th>
            <th>Preço</th>

            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $s)
            <tr>
                <td>{{$s->id}}</td>
                <td>{{$s->name}}</td>
                <td>{{$s->price}}</td>
                <td>

                    <a href="{{route('admin.products.edit',['product'=>$s->id])}}"  class="btn btn-sm btn-primary"> Editar</a>
                    <a href="{{route('admin.products.destroy',['product'=>$s->id])}}"  class="btn btn-sm btn-danger"> Deletar</a>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{$products->links()}}

@endsection
