<?php


namespace App\Traits;


use Illuminate\Http\Request;

trait UploadTrait
{
    private function uploadphoto(Request $request, $imageColumn = null){
        $fotos = [];
        $images = $request->file('photos');

        if(!is_null($imageColumn)){
            foreach ($images as $i){
                if(!is_null($imageColumn)){
                    $fotos [] = [$imageColumn => $i->store('products','public')];
                }


            }
        }
        else{
            $fotos = $images->store('logo','public');
        }

        return $fotos;
    }

}
