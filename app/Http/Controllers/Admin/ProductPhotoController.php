<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ProductPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductPhotoController extends Controller
{
    public function remove_photo(Request $request){
        $link = $request->get('photo');
        if(Storage::disk('public')->exists($link)){
            Storage::disk('public')->delete($link);
            $photo = ProductPhoto::where('image', $link);
            $id = $photo->first()->product_id;
            $photo->delete();

            flash('imagem removida com sucesso!')->success();

            return redirect()->route('admin.products.edit',['product'=> $id]);
        }

    }
}
