<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Store;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRequest;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Storage;

class StoreController extends Controller
{
    //
    use UploadTrait;
    private $store;
    public function __construct(Store $store)
    { //As rotas de middlewars ficam no arquivo kernel.php
        //$this->middleware('user.has.store')->only(['create','store']);
        $this->middleware('auth');
        $this->store = $store;
    }

    public function single($slug)
    {
        $store = $this->store->where('slug',$slug)->first();
        return view('storesingle',compact('store'));
    }

    public function index()
    {
        //$stores = \App\Store::paginate(10);
        //dd(auth()->user()->stores);
        $store = auth()->user()->stores;
        return view('admin.stores.index',compact('store'));
    }

    public function create()
    {

        $users = \App\User::all(['id','name']);
        return view('admin.stores.create',compact('users'));
    }

    public function store(StoreRequest $request)
    {
        $data = $request->all();
        //$user = \App\User::findOrFail($data['user']);


       if($request->hasFile('photos')){
           $data['logo'] =$this->uploadphoto($request);
       }

        $user = auth()->user();
        $store = $user->stores()->create($data);
        flash('Loja criada com sucesso')->success();
        return redirect()->route('admin.stores.index');

    }

    public function edit($store)
    {
        $store = \App\Store::findOrFail($store);
        return view ('admin.stores.edit',compact('store'));
    }

    public function update (StoreRequest $request, $store)
    {
        $store = \App\Store::findOrFail($store);
        $data = $request->all();
        if($request->hasFile('photos')){
            if(Storage::disk('public')->exists($store->logo)){
                Storage::disk('public')->delete($store->logo);

            }
            $data['logo'] =$this->uploadphoto($request);
        }

    $store->update($data);
    flash('Loja atualizada com sucesso')->success();
    return redirect()->route('admin.stores.index');
    }

    public function delete ($store){
        $store = \App\Store::findOrFail($store);
        $store->delete();
        flash('Loja deletada com sucesso')->success();
        return redirect()->route('admin.stores.index');;
    }
}
