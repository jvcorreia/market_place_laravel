<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductsRequest;
use App\Product;
use Illuminate\Http\Request;


class ProductsController extends Controller
{
    private $product;
    public function __construct(Product $product){
        $this->product = $product;
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userStore = auth()->user()->stores;
        $products = $userStore->products()->paginate(10);
        //$products = $this->product->paginate(10);

        return view('admin.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = \App\Category::all(['id','name']);
        $stores = \App\Store::all(['id','name']);
        return view('admin.products.create',compact(['categories','stores']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsRequest $request)
    {


        $data = $request->all();


        $categories = $request->get('categories',null);
        //$store = \App\Store::find($data['store']);
        $store = auth()->user()->stores;
        $product = $store->products()->create($data);


        $product->categories()->sync($categories);

        if($request->hasFile('photos')){
                $images = $this->uploadphoto($request, 'image');
                //Inserção no banco
            $product->photos()->createMany($images);
        }


        flash('Produto criado com sucesso!')->success();
        return redirect()->route('admin.products.index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->product->find($id);
        return view('admin.products.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = \App\Category::all(['id','name']);
        $product = $this->product->find($id);
        return view('admin.products.edit',compact(['product','categories']));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductsRequest $request, $id)
    {
        $data = $request->all();
        $product = $this->product->find($id);
        $product->update($data);
        $categories = $request->get('categories',null);
        if(!is_null($categories)){
            $product->categories()->sync($categories);
        }

        if($request->hasFile('photos')){
            $images = $this->uploadphoto($request, 'image');
            //Inserção no banco
            $product->photos()->createMany($images);
        }
        flash('Produto atualizado com sucesso!');
        return redirect()->route('admin.products.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->product->find($id);
        $product->delete();
        flash('Produto removido com sucesso!');
        return redirect()->route('admin.products.index');
    }

    private function uploadphoto(Request $request, $imageColumn){
        $fotos = [];
        $images = $request->file('photos');
        foreach ($images as $i){
            $fotos [] = [$imageColumn => $i->store('products','public')];
        }

        return $fotos;
    }
}
