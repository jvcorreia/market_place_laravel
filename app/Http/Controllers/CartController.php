<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{

    public function index()
    {
        return view('cart');
    }

    public function add(Request $request)
    {
        $productData = $request->get('product');

        $product = \App\Product::where('slug',$productData['slug']);

        if(!$product->count()){
            return redirect()->route('home');
        }

        $product = $product->first(['name','price'])->toArray();

        $product = array_merge($product, $productData);

        //Verifica se existe sessão p o produto
        if(session()->has('cart')){ //Se existe ele so adiciona, se nao criar a sessao

            $products = session()->get('cart');
            $productsSlugs = array_column($products, 'slug');
            if(in_array($product['slug'], $productsSlugs)){
                $this->productIncrement($product['slug'], $product['amount'], $products);
                session()->put('cart',$products);
            }else{
                session()->push('cart', $product);
            }

        }else{
            $products[] = $product;
            session()->put('cart',$products);
        }

        flash('Produto adicionado no carrinho')->success();

        return redirect()->route('product.single',['slug'=>$product['slug']]);
    }

    public function remove($slug)
    {
        if(!session()->has('cart')){
            return redirect()->route('cart.index');
        }
        $products = session()->get('cart');
        $products = array_filter($products, function ($line) use($slug){
            return $line['slug'] != $slug;
        });

        session()->put('cart', $products);

        flash('Produto removido no carrinho')->success();

        return redirect()->route('cart.index');
    }

    public function cancel()
    {
        session()->forget('cart');
        flash('Carrinho cancelado')->info();
        return redirect()->route('cart.index');

    }

    private function productIncrement($slug, $amount,  $products)
    {
            $products = array_map(function ($line) use ($slug, $amount){
               if($slug == $line['slug']){
                   $line['amount'] += $amount;
               }
               return $line;
            }, $products);
            return $products;
    }
}
