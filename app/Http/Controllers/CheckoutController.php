<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index(){
        if(auth()->check()){
            session()->forget('cart');
            return view('checkout');
        }else{
            return redirect()->route('login');
        }
    }
}
