<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $product;
    public function __construct(Product $product)
    {

        $this->middleware('auth');
        $this->product = $product;

    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = $this->product->limit(6)->orderBy('id','DESC')->get();
        $stores = \App\Store::limit(3)->get();

        return view('welcome', compact('products','stores'));
        //Compartilhar dados entre todas as views
    }


    public function single($slug)
    {
        $product = $this->product->where('slug',$slug)->first();
        return view('single', compact('product'));

    }
}
